﻿using System;
using System.Text;

namespace stack
{
    public class Caculator
    {
        private static bool Predecessor(char firstOperator, char secondOperator)
        {
            string opString = "(+-*/%";

            int firstPoint, secondPoint;

            int[] precedence = { 0, 12, 12, 13, 13, 13 };// "(" has less prececence

            firstPoint = opString.IndexOf(firstOperator);
            secondPoint = opString.IndexOf(secondOperator);

            return (precedence[firstPoint] >= precedence[secondPoint]) ? true : false;
        }
        /*
         * Convert Infix to Postfix
         * if char is number we will write to result
         * if char is '(' we will push stack 
         * if char is operator we will perform 2:
         *      + if stack doesn't empty we will compare top(t2) in stack with my operator(t1)
         *          * t1 <= t2: push t2 to result 
         *      + if stack empty or or t2 <= t1 push t1 to stack
         * if char is ')' we will push all operator to stack into meet ')' or stack empty
         * when ww browse all infix we push all number in stack
         */

        public static string ConvertToPostFix(string inFix)
        {
            StringBuilder postFix = new StringBuilder();
            char arrival;
            Stack<char> oprerator = new Stack<char>();//Creates a new Stack
            foreach (char c in inFix.ToCharArray())//Iterates characters in inFix
            {
                if (Char.IsNumber(c))
                    postFix.Append(c);
                else if (c == '(')
                    oprerator.push(c);
                else if (c == ')')//Removes all previous elements from Stack and puts them in. front of PostFix.  
                {
                    arrival = oprerator.pop();
                    while (arrival != '(')
                    {
                        postFix.Append(arrival);
                        arrival = oprerator.pop();
                    }
                }
                else
                {
                    if (oprerator.size() != 0 && Predecessor(oprerator.peek(), c))//If find an operator
                    {
                        arrival = oprerator.pop();
                        while (Predecessor(arrival, c))
                        {
                            postFix.Append(arrival);

                            if (oprerator.size() == 0)
                                break;

                            arrival = oprerator.pop();
                        }
                        oprerator.push(c);
                    }
                    else
                        oprerator.push(c);//If Stack is empty or the operator has precedence 
                }
            }
            while (oprerator.size() > 0)
            {
                arrival = oprerator.pop();
                postFix.Append(arrival);
            }
            return postFix.ToString();
        }


    }
}