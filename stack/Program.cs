﻿using System;

namespace stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            stack.push(1);
            stack.push(2);
            Console.WriteLine(stack.size());
            Console.WriteLine(stack.peek());
            Console.WriteLine(stack.pop());
            Console.WriteLine(stack.peek());
            Console.WriteLine(stack.size());
            Console.WriteLine(stack.pop());
            Console.WriteLine(stack.size());
            //// test convert infix to posfix

            Console.Write("Enter operator: ");
            string infix = Console.ReadLine();
            Console.WriteLine("Posfix is: " + Caculator.ConvertToPostFix(infix));
            Console.ReadKey();
        }
    }
}
