﻿using System;
namespace stack
{
    public class Node<T>
    {
        T data;
        Node<T> next;
        public Node(){}

        public T Data { get => data; set => data = value; }
        public Node<T> Next { get => next; set => next = value; }
    }
}
