﻿using System;
namespace stack
{
    public class Stack<T>
    {
        private static int ELEMENT_FIRST = 0;
        private LinkList<T> linkList;
        public Stack()
        {
            linkList = new LinkList<T>();
        }

        public bool isEmpty()
        {
            return this.linkList.Size == 0;
        }

        public void push(T data)
        {
            this.linkList.insertFirst(data);
        }

        public T pop()
        {
            T data = this.linkList.getNode(ELEMENT_FIRST).Data;
            this.linkList.remove(ELEMENT_FIRST);
            return data;
        }

        public T peek()
        {
            return this.linkList.getNode(ELEMENT_FIRST).Data;
        }

        public void printStack()
        {

        }

        public int size()
        {
            return this.linkList.Size;
        }
    }
}
