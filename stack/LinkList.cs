﻿using System;
namespace stack
{

    public class LinkList<T>
    {
        private Node<T> head;
        private int size;

        public LinkList()
        {
            this.size = 0;
            this.head = null;
        }

        public Node<T> Head { get => head; set => head = value; }
        public int Size { get => size; set => size = value; }

        public void insertFirst(T data)
        {
            Node<T> tmp = new Node<T>();
            tmp.Data = data;
            tmp.Next = head;
            head = tmp;
            size++;
        }

        public void insertEnd(T data)
        {
            Node<T> newNode = new Node<T>();
            newNode.Data = data;
            newNode.Next = null;
            if (head == null)
            {
                head = newNode;
            }
            else
            {
                Node<T> current = head;
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = newNode;
            }
            size++;
        }

        public void insert(T data, int pos)
        {
            if (pos < 0 || pos > size)
            {
                throw new ArgumentException("Position not allow");
            }
            Node<T> newNode = new Node<T>();
            newNode.Data = data;
            newNode.Next = null;
            Node<T> current = head;
            while (pos > 0)
            {
                current = current.Next;
                pos--;
            }
            newNode.Next = current.Next;
            current.Next = newNode;
            size++;
        }


        public bool remove(int pos)
        {
            if (pos < 0 || pos > size)
            {
                return false;
            }

            Node<T> current = head;
            if (pos == 0)
            {
                head = current.Next;
            }
            else
            {
                int i = 0;
                while (i < pos - 1)
                {
                    current = current.Next;
                    i++;
                }
                current.Next = current.Next.Next;
            }
            size--;
            return true;
        }

        public Node<T> getNode(int pos)
        {
            if (pos < 0 || pos > size)
            {
                return null;
            }
            int i = 0;
            Node<T> current = head;
            while (i < pos)
            {
                current = current.Next;
                i++;
            }
            return current;
        }

        public void destroy()
        {
            head = null;
            size = 0;
        }
    }
}
